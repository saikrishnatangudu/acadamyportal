package com.hcl.controller;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.hcl.model.Course;
import com.hcl.model.Student;
import com.hcl.service.StudentServiceImpl;


	@RunWith(MockitoJUnitRunner.Silent.class)
	public class StudentControllerTest {
		@InjectMocks
		StudentController studentController;
		
		@Mock
		StudentServiceImpl studentServiceImpl;
		
		static Student student ;
		static Course course;
		
		@BeforeClass
		public static void setup() {
			
			Set<Course> courses = new HashSet<Course>();
			courses.add(course);
			course= new Course();
			student= new Student();
			student.setStudentId(123);
			student.setStudentName("saikrishna");
			//student.setCourses(courses);

		}
		
		@Test
		public void testReadById() {

			Mockito.when(studentServiceImpl.getStudent(student.getStudentId())).thenReturn(student);

			ResponseEntity<Student> p1 = studentController.getStudent(student.getStudentId());
			Assert.assertNotNull(p1);

		}
		
		
		@Test
		public void testGetAll() {
			List<Student> students= new ArrayList<Student>();
			students.add(student);

			Mockito.when(studentServiceImpl.getAllStudents()).thenReturn(students);

			ResponseEntity<List<Student>> p1 = studentController.getAllStudents();
			Assert.assertNotNull(p1);

		}
		
		@Test
		public void testReadByIdpositive() {

			Mockito.when(studentServiceImpl.getStudent(student.getStudentId())).thenReturn(student);

			ResponseEntity<Student> p1 = studentController.getStudent(student.getStudentId());
			Assert.assertEquals(HttpStatus.OK, p1.getStatusCode());

		}
		
		
		@Test
		public void testGetAllpositive() {
			List<Student> students= new ArrayList<Student>();
			students.add(student);

			Mockito.when(studentServiceImpl.getAllStudents()).thenReturn(students);

			ResponseEntity<List<Student>> p1 = studentController.getAllStudents();
			Assert.assertEquals(HttpStatus.OK, p1.getStatusCode());
		}
}

