package com.hcl.controller;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.aspectj.weaver.NewConstructorTypeMunger;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.hcl.model.Course;
import com.hcl.model.Student;
import com.hcl.service.CourseServiceImpl;




	@RunWith(MockitoJUnitRunner.Silent.class)
	public class CourseControllerTest {
		@InjectMocks
		CourseController courseController;
		
		@Mock
		CourseServiceImpl courseServiceImpl;
		
		static Student student =null;
		static Course course=null;
		
		@BeforeClass
		public static void setup() {
			
			Set<Student> students = new HashSet<Student>();
			student = new Student();
			course=new Course();
			course.setCourseId(12);
			course.setCourseName("key");
			course.setStudents(students);;

		}
		
		@Test
		public void testReadById() {

			Mockito.when(courseServiceImpl.getCourseById(course.getCourseId())).thenReturn(course);


			ResponseEntity<Course> p1 = courseController.getCourseById((student.getStudentId()));
			Assert.assertNotNull(p1);

		}
		
		
		@Test
		public void testGetAll() {
			List<Course> courses= new ArrayList<Course>();
			courses.add(course);

			Mockito.when(courseServiceImpl.getAllCourses()).thenReturn(courses);

			ResponseEntity<List<Course>> p1 = courseController.getAllCourese();
			Assert.assertNotNull(p1);

		}
		
		@Test
		public void testReadByIdpositive() {

			Mockito.when(courseServiceImpl.getCourseById(course.getCourseId())).thenReturn(course);


			ResponseEntity<Course> p1 = courseController.getCourseById((student.getStudentId()));
			Assert.assertEquals(HttpStatus.OK, p1.getStatusCode());;

		}
		
		
		@Test
		public void testGetAllPositive() {
			List<Course> courses= new ArrayList<Course>();
			courses.add(course);

			Mockito.when(courseServiceImpl.getAllCourses()).thenReturn(courses);

			ResponseEntity<List<Course>> p1 = courseController.getAllCourese();
			Assert.assertEquals(HttpStatus.OK, p1.getStatusCode());;

		}
		
}

