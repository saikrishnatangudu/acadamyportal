package com.hcl.service;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import com.hcl.model.Course;
import com.hcl.model.Student;
import com.hcl.repositary.CourseRepositary;

	@RunWith(MockitoJUnitRunner.Silent.class)
	public class CourseServiceTest {
		@InjectMocks
		CourseServiceImpl courseServiceImpl;
		
		@Mock
		CourseRepositary courseRepositary;
		
		static Student student;
		static Course course;
		
		
		@BeforeClass
		public static void setup() {
			
			Set<Student> students = new HashSet<Student>();
			student= new Student();
			students.add(student);
			course=new Course();
			course.setCourseId(12);
			course.setCourseName("key");
			course.setStudents(students);;

		}
		
		@Test
		public void testReadById() {

			Mockito.when(courseRepositary.findById(course.getCourseId())).thenReturn(Optional.of(course));


			Course p1 = courseServiceImpl.getCourseById((course.getCourseId()));
			Assert.assertNotNull(p1);

		}
		
		
		@Test
		public void testGetAll() {
			List<Course> courses= new ArrayList<Course>();
			courses.add(course);

			Mockito.when(courseRepositary.findAll()).thenReturn(courses);

			List<Course> p1 = courseServiceImpl.getAllCourses();
			Assert.assertNotNull(p1);

		}
		
		@Test
		public void testReadByIdpositive() {

			Mockito.when(courseRepositary.findById(course.getCourseId())).thenReturn(Optional.of(course));


			Course p1 = courseServiceImpl.getCourseById((course.getCourseId()));
			Assert.assertEquals(p1, course);

		}
		
		
		@Test
		public void testGetAllPositive() {
			List<Course> courses= new ArrayList<Course>();
			courses.add(course);

			Mockito.when(courseRepositary.findAll()).thenReturn(courses);

			List<Course> p1 = courseServiceImpl.getAllCourses();
			Assert.assertEquals(p1, courses);

		}
		
		
}
