package com.hcl.repositary;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.hcl.model.Course;
@Repository
public interface CourseRepositary extends JpaRepository<Course, Integer>{


}
